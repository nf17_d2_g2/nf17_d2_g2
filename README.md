# NF17 PROJET

- Sujet : Bibliothèque
- Chargé de TD : Alessandro Victorino 
- Semestre : P20

### Membres du groupe 
- Laurène Coyen
- Zhuofan Xu
- Pierre Pouliquen
- Quentin Leprat
- Romane Guari 

### Contenu du livrable
- README
- NDC
- MCD
- MLD relationnel
- BDD : tables et vues, données de test, questions attendues 

