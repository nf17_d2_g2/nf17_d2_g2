CREATE TABLE Ressources (
	R_code 		int PRIMARY KEY,
	titre		text NOT NULL,
	date_apparition	date,
	editeur		text,
	genre		text,
	code_class	text
);

CREATE TABLE Livre (
	code 		int REFERENCES Ressources(R_code),
	ISBN		text NOT NULL,
	langue		text NOT NULL,
	resume		text,
	PRIMARY KEY (code)
);

CREATE TABLE Film (
	code 		int REFERENCES Ressources(R_code),
	langue		text,
	duree		interval NOT NULL,
	synopsis	text,
	PRIMARY KEY (code)
);

CREATE TABLE EnregistrementMusical (
	code 		int REFERENCES Ressources(R_code),
	duree		interval NOT NULL,
	PRIMARY KEY (code)
);

CREATE TABLE Exemplaire (
	numero		int NOT NULL,
    code        int REFERENCES Ressources(R_code),
    etat        text,
    dispo       boolean,
    
    CONSTRAINT check_etat
        	CHECK (etat IN ('neuf', 'bon','abime','perdu')),
    PRIMARY KEY (numero, code)
    
);



CREATE TYPE type AS ENUM('interprete','acteur','realisateur','auteur','compositeur');

CREATE TABLE Contributeur (
    id		        int PRIMARY KEY,
	nom	    	    text NOT NULL,
	prenom		    text NOT NULL,
	date_naissance	date NOT NULL,
	nationalite	    text NOT NULL,
	t		        type NOT NULL
	
);


CREATE TABLE ContributionFilm(
	id		int,
	code	int,
	PRIMARY KEY (id,code),
	FOREIGN KEY (id) REFERENCES Contributeur(id),
	FOREIGN KEY (code) REFERENCES Film(code)
);

CREATE TABLE ContributionEnregistrementMusical(
	id		int,
	code	int,
	PRIMARY KEY (id,code),
	FOREIGN KEY (id) REFERENCES Contributeur(id),
	FOREIGN KEY (code) REFERENCES EnregistrementMusical(code)
);


CREATE TABLE ContributionLivre(
	id		int,
	code 	int,
	PRIMARY KEY (id,code),
	FOREIGN KEY (id) REFERENCES Contributeur(id),
	FOREIGN KEY (code) REFERENCES Livre(code)
);



CREATE TABLE CompteUtilisateur (
	login           char(20) PRIMARY KEY,
	mdp             text UNIQUE NOT NULL,
	nom             text,
	prenom          text,
	adresse         text,
	adresse_mail	text
);

CREATE TABLE Personnel (
	login		char(20) PRIMARY KEY,
	FOREIGN KEY(login) REFERENCES CompteUtilisateur(login)
);

CREATE TABLE Adherents ( 
	numero_tel 	    text,
	carte           text NOT NULL	CHECK (carte IN ('active', 'inactive')),
	nb_prets        integer	NOT NULL CHECK (nb_prets < 10), --10 = valeur arbitraire
	suspendu        boolean NOT NULL,
    blacklist       boolean NOT NULL,
	login		    char(20) PRIMARY KEY,
	FOREIGN KEY(login) REFERENCES CompteUtilisateur(login)	      	
);



CREATE TABLE Pret (
    date_emprunt 	date,
	duree_jours	    int,
	login 		    char(20),
	num_exemplaire  int,
	code 		    int,
	PRIMARY KEY (date_emprunt,login,code),
	FOREIGN KEY (login) REFERENCES Adherents(login),
	FOREIGN KEY (num_exemplaire, code) REFERENCES Exemplaire(numero,code)
);



CREATE TABLE Retard (
    date_debut date,
	nb_jours	int,
    id int PRIMARY KEY 
);

CREATE TABLE Degradation (
    date_debut date,
	montant		decimal,
    remboursement  	boolean,
    id int PRIMARY KEY
	
);


CREATE TABLE Sanction (
    id       int PRIMARY KEY,
    login    char(20) NOT NULL,
    retard_id int,
    degradation_id int,
    FOREIGN KEY (login) REFERENCES Adherents(login),
    FOREIGN KEY (retard_id) REFERENCES Retard(id),
    FOREIGN KEY (degradation_id) REFERENCES Degradation(id)
);



CREATE VIEW vInterprete
AS SELECT *
FROM Contributeur
WHERE Contributeur.t='interprete';

CREATE VIEW vAuteur
AS SELECT *
FROM Contributeur
WHERE Contributeur.t='auteur'; 

CREATE VIEW vRealisateur
AS SELECT *
FROM Contributeur
WHERE Contributeur.t='realisateur'; 

CREATE VIEW vActeur
AS SELECT *
FROM Contributeur
WHERE Contributeur.t='acteur'; 

CREATE VIEW vCompositeur
AS SELECT *
FROM Contributeur
WHERE Contributeur.t='compositeur'; 

CREATE VIEW vLivre AS 
SELECT R_code,titre, date_apparition, editeur, genre, code_class, ISBN, langue, resume 
FROM Ressources RIGHT OUTER JOIN Livre ON Ressources.R_code = Livre.code
ORDER BY Ressources.R_code;

CREATE VIEW vFilm AS 
SELECT R_code,titre, date_apparition, editeur, genre, code_class, langue, duree, synopsis 
FROM Ressources RIGHT OUTER JOIN Film ON Ressources.R_code = Film.code
ORDER BY Ressources.R_code;

CREATE VIEW vEnregistrementMusical AS 
SELECT R_code,titre, date_apparition, editeur, genre, code_class, duree
FROM Ressources RIGHT OUTER JOIN EnregistrementMusical ON Ressources.R_code = EnregistrementMusical.code
ORDER BY Ressources.R_code;

CREATE VIEW vExemplaire AS
SELECT R_code,titre, numero, date_apparition, editeur, genre, code_class, etat, dispo
FROM Ressources RIGHT OUTER JOIN Exemplaire ON Ressources.R_code = Exemplaire.code
ORDER BY Ressources.R_code, Exemplaire.numero;


CREATE VIEW vStatistiquesAdherents AS
SELECT C.nom, C.prenom, R.genre
FROM CompteUtilisateur C, Pret P, Ressources R
WHERE C.login = P.login
AND R.r_code = P.code
GROUP BY C.nom, C.prenom, R.genre
HAVING COUNT(R.genre)>4
ORDER BY COUNT(R.genre);

CREATE VIEW cCondition_exemplaire AS
SELECT etat, COUNT(etat)
FROM Exemplaire
GROUP BY etat;

CREATE VIEW cDispo_exemplaire AS
SELECT dispo, COUNT(dispo)
FROM Exemplaire
GROUP BY dispo;

CREATE VIEW vRessources_populaires AS
SELECT R_code, titre, date_apparition, genre, code_class, COUNT(R_code)
FROM Pret LEFT OUTER JOIN Ressources ON Pret.code = Ressources.R_code
GROUP BY Ressources.R_code
ORDER BY COUNT(R_code) DESC;



INSERT INTO Contributeur VALUES (1, 'p','n','12-09-1789','francais','auteur');
INSERT INTO Contributeur VALUES (2, 'p1','n1','12-09-1989','francais','acteur');
INSERT INTO Contributeur VALUES (3, 'p2','n2','12-09-1999','francais','realisateur');
INSERT INTO Contributeur VALUES (4, 'p3','n','12-09-1999','francais','interprete');
INSERT INTO Contributeur VALUES (5, 'p4','n','12-09-1999','francais','compositeur');




INSERT INTO Ressources
VALUES (1,'VITAMINA A1','2020-01-01','Who?','Horreur','1111'), (2,'Demain', '2016-11-01','Who again','Romantique','1122'), 
(3,'Les Miserables', '2012-12-05', 'Who!', 'Drama', '1133'), (4,'Take Me to Church', '2013-01-01','Who','Soul','1144'),
(5,'Les Evades','1995-03-01','','Drama','1133'),(6,'Le Parrain','1972-10-18','','Crime','1133');

INSERT INTO Livre
VALUES (1,'9788416782352', 'Espagnol','Un curso blablabal'),
(2,'9782266276276', 'Francais', 'Emma vit a New York. etc');

INSERT INTO Film
VALUES (3,'Anglais', '2 hour 38 minutes', 'Blabla'),(5,'Anglais', '2 hours 22 minutes','Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.'),
(6,'Anglais','2 hours 55 minutes', 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.');

INSERT INTO EnregistrementMusical
VALUES (4,'4 minutes 1 second');

INSERT INTO Exemplaire
VALUES (1,1,'bon',true),(1,2,'abime',false),(2,2,'neuf',true),(1,3,'bon',true),(1,4,'perdu',false),
(1,5,'bon',true),(1,6,'bon',true);

INSERT INTO ContributionFilm VALUES (2,3),(3,3),(2,6);
INSERT INTO ContributionLivre VALUES (1,1),(1,2);
INSERT INTO ContributionEnregistrementMusical VALUES (5,4),(4,4);



INSERT INTO CompteUtilisateur(login, mdp, nom, prenom, adresse, adresse_mail)
VALUES ('dupondtom', 'tom123', 'Dupond', 'Tom', '3 rue de la liberte', 'tom.dupond@orange.fr');
INSERT INTO Personnel(login)
VALUES ('dupondtom');

INSERT INTO CompteUtilisateur(login, mdp, nom, prenom, adresse, adresse_mail)
VALUES ('durandmarie', 'marie145', 'Durand', 'Marie', '3 rue de la liberte', 'marie.durand@hotmail.fr');
INSERT INTO Personnel(login)
VALUES ('durandmarie');


INSERT INTO CompteUtilisateur(login, mdp, nom, prenom, adresse, adresse_mail)
VALUES ('cocoju', 'julie98', 'Coco', 'Julie', '19 impasse de la laverie', 'julie.coco@outlook.fr');
INSERT INTO CompteUtilisateur(login, mdp, nom, prenom, adresse, adresse_mail)
VALUES ('gaucherro', 'gau43RE', 'Gaucher', 'Roger', '45 avenue de Paris', 'roger.gaucher@wanadoo.fr');
INSERT INTO CompteUtilisateur(login, mdp, nom, prenom, adresse, adresse_mail)
VALUES ('DupuisMa', 'GTEV5T', 'Dupuis', 'Martin', '6 rue de la fayette', 'martin.dupuis@wanadoo.fr');

INSERT INTO Adherents(login, numero_tel, carte, nb_prets, suspendu, blacklist)
VALUES ('cocoju', '0164762343', 'active', 3, FALSE, FALSE);
INSERT INTO Adherents(login, numero_tel, carte, nb_prets, suspendu, blacklist)
VALUES ('dupondtom', '0345678797', 'inactive', 4, FALSE, FALSE);
INSERT INTO Adherents(login, numero_tel, carte, nb_prets, suspendu, blacklist)
VALUES ('gaucherro', '0143554353', 'active', 3, TRUE, FALSE);
INSERT INTO Adherents(login, numero_tel, carte, nb_prets, suspendu, blacklist)
VALUES ('DupuisMa', '0444435524524', 'active', 3, TRUE, TRUE);

INSERT INTO Retard(date_debut,nb_jours, id)
VALUES (TO_DATE('20200316','YYYYMMDD'),0,1118);
INSERT INTO Retard(date_debut,nb_jours, id)
VALUES (TO_DATE('20200312','YYYYMMDD'),3,8);
INSERT INTO Retard(date_debut,nb_jours, id)
VALUES (TO_DATE('20200216','YYYYMMDD'),200,45);
INSERT INTO Retard(date_debut,nb_jours, id)
VALUES (TO_DATE('20200116','YYYYMMDD'),3,4);
INSERT INTO Degradation(date_debut,montant, remboursement,id)
VALUES (TO_DATE('20200206','YYYYMMDD'),4500,False,01);
INSERT INTO Degradation(date_debut,montant, remboursement,id)
VALUES (TO_DATE('20200306','YYYYMMDD'),1,True,00000002);
INSERT INTO Degradation(date_debut,montant, remboursement,id)
VALUES (TO_DATE('20200125','YYYYMMDD'),0,False,145);

INSERT INTO Sanction(id, login, retard_id)
VALUES (001,'DupuisMa', 004);
INSERT INTO Sanction(id, login, retard_id)
VALUES (002,'DupuisMa', 8);
INSERT INTO Sanction(id, login, retard_id,degradation_id)
VALUES (003,'DupuisMa', 1118,1);
INSERT INTO Sanction(id, login,degradation_id)
VALUES (314159265,'cocoju',1);
INSERT INTO Sanction(id, login,degradation_id)
VALUES (7,'cocoju',2);

INSERT INTO Pret
VALUES(TO_DATE('20170516','YYYYMMDD'), 20, 'cocoju', 1,2);
INSERT INTO Pret
VALUES(TO_DATE('20171124','YYYYMMDD'), 20, 'dupondtom', 1,3);
INSERT INTO Pret
VALUES(TO_DATE('20180617','YYYYMMDD'), 20, 'cocoju', 1,1);
INSERT INTO Pret
VALUES(TO_DATE('20181220','YYYYMMDD'), 20, 'dupondtom', 1,6);
INSERT INTO Pret
VALUES(TO_DATE('20190115','YYYYMMDD'), 20, 'gaucherro', 1,5);
INSERT INTO Pret
VALUES(TO_DATE('20191013','YYYYMMDD'), 20, 'dupondtom', 1,4);
INSERT INTO Pret
VALUES(TO_DATE('20191130','YYYYMMDD'), 20, 'cocoju', 1,2);
INSERT INTO Pret
VALUES(TO_DATE('20200106','YYYYMMDD'), 30, 'gaucherro', 1,3);
INSERT INTO Pret
VALUES(TO_DATE('20200203','YYYYMMDD'), 30, 'DupuisMa', 1,1);
INSERT INTO Pret
VALUES(TO_DATE('20200301','YYYYMMDD'), 10, 'dupondtom', 1, 5);
INSERT INTO Pret
VALUES(TO_DATE('20200306','YYYYMMDD'), 15, 'dupondtom', 1, 3);
INSERT INTO Pret
VALUES(TO_DATE('20200315','YYYYMMDD'), 30, 'dupondtom', 1, 5);
INSERT INTO Pret
VALUES(TO_DATE('20200415','YYYYMMDD'), 15, 'dupondtom', 1, 5);



