                   
# Note de clarification - projet Bibliothèque
- Client : Alessandro Victorino
- Objectif : concevoir un système de gestion pour une bibliothèque municipale qui souhaite informatiser ses activités : catalogage, consultations, gestion des utilisateurs, prêts, etc.

## Liste des objets qui devront être gérés dans la base de données
- Ressource : livres, films, enregistrements musicaux
- Contributeur
- Personne : membre du personnel, adhérent
- Prêt

## Liste des propriétés associées à chaque objet
#### Ressource
Une ressource possède code unique, un titre, une liste de contributeurs, une date d'apparition, un éditeur, un genre et un code de classification permettant de le localiser dans la bibliothèque. Une ressource est un :
- Livre possédant un ISBN, une langue d’écriture et un résumé
- Film possédant une langue, une longueur (durée) et un synopsis
- Enregistrement musical possédant longueur (durée)

Une ressource peut exister en plusieurs exemplaires. Un exemplaire d’une ressource possède un état : neuf, bon, abîmé, perdu.

#### Contributeur 
Un contributeur a un nom, un prénom, une date de naissance et une nationalité.

#### Compte utilisateur 
Un compte utilisateur est composé d’un login, d’un mot de passe, du nom, du prénom, de l’adresse et de l’adresse email de l’utilisateur en question. Il y a deux types de compte :
- les comptes du personnel ayant accès aux fonctions d’administrations
- les comptes des adhérents avec des informations supplémentaire : un numéro de téléphone et une carte d’adhérent permettant l’emprunt des ressources. Cette carte d’adhérent peut être active ou inactive afin de garder une trace des adhésions passées.

#### Prêt 
Un prêt est caractérisé par une date de prêt et une durée de prêt.

## Liste des contraintes associées à ces objets et propriétés

#### Contraintes sur un objet ressource :
- Pour un livre, les contributeurs sont les auteurs.
- Pour une oeuvre musicale : les contributeurs sont les compositeurs et les interprètes.
- Pour un film : les contributeurs sont les acteurs et les réalisateurs.
- Une ressource ne peut être emprunté que s’il est disponible et en bon état.

#### Contraintes sur prêt :
- Un adhérent peut emprunter un nombre limité d’oeuvres en une seule fois, chacune pour une durée limitée.
- Pour pouvoir emprunter un document, un adhérent à besoin de s'authentifier.
- Un adhérent sera sanctionné pour un retard de retour d’un ouvrage, pour sa dégradation et sa perte :
    - Une sanction pour retard est une suspension de droit de prêt d’une durée égale au nombre de jours de retard.
    - Une sanction pour dégradation ou perte est une suspension de droit de prêt jusqu’à ce que l’adhérent rembourse le document en question.
    - Les sanctions répétées pour un adhérent entraînent le placement de son nom sur une backlist.

## Liste des utilisateurs appelés à modifier et consulter les données
Les membres du personnel de la bibliothèque seront appelés à modifier et à consulter les données. Ils doivent être en capacité de :
- gérer les adhérents et leurs données 
- gérer les prêts, les retards et les réservations
- gérer les ressources documentaires 

Les adhérents pourront rechercher des documents et gérer leurs emprunts personnels.

## Liste des fonctions que ces utilisateurs pourront effectuer
Un membre du personnel aura accès aux fonctions suivantes :
- modifier l’état de disponibilité de la ressource,
- ajouter, modifier, supprimer des adhérents,
- ajouter, supprimer et modifier des ressources,
- modifier l’état d’un exemplaire d’une ressource,
- enregistrer un nouvel emprunt.


Les adhérents auront accès aux fonctions suivantes :
- recherche d’une ressource avec un accès à son état et sa disponibilité,
- consultation de son profil,
- consultation des ressources empruntés et jusqu’à quelle date.
